class Sigle:
    def __init__(self, nom, description):
        self.nom = nom
        self.description = description

    def setNom(self, nom):
        self.nom = nom

    def getNom(self):
        return self.nom

    def setDescription(self, description):
        self.description = description

    def getDescription(self):
        return self.description


def menu_principal():
    print("***************************************************************")
    print("*                Menu principal                               *")
    print("*        1.Afficher les sigles et leur significations         *")
    print("*        2.Rechercher un sigle                                *")
    print("*        3.Générer un sigle                                   *")
    print("*        4.Ajouter un sigle                                   *")
    print("*        5.Exit/Quit                                          *")
    print("***************************************************************")
    return 0


def option1():
    print("***************************************************************")
    print("*        1.Afficher les sigles et leurs significations        *")
    print("*    Nous avons les sigles et les significations suivants     *")
    print("***************************************************************")
    with open("descriptions.txt", "r") as file:
        for line in file:
            print(line, end="")
    return 0


def option2():
    print("***************************************************************")
    print("*                    2.Rechercher un sigle                    *")
    print("***************************************************************")
    print(" Saisir le sigle en MAJUSCULES.")
    elem = input(" Entrer un sigle à rechercher svp: ")
    with open("descriptions.txt", "r") as file:
        lines = file.readlines()
        trouver = False
        tab = []
        for line in lines:
            tab = line.split(":")
            if (elem in tab[0]):
                trouver = True
                print("\n Sigle trouvé: ", end="")
                print(line, end="")
            else:
                continue
    if trouver == False:
        print("\n Désolé, ce sigle n'existe pas ddans notre fichier.")
    return 0


def option3():
    print("***************************************************************")
    print("*                     3.Générer un sigle                      *")
    print("***************************************************************")
    print(" Entrer une description de sigle: ")
    description = input(" Avec la lettre devant être considérée dans le sigle en majuscule svp: ")
    nom = ""
    for word in description:
        for char in word:
            if char.isupper():
                nom += char
            else:
                continue
    print("\t Ce qui nous donne le sigle: ", nom)
    # Enrégistrer le sigle généré
    with open("descriptions.txt", "a") as file:
        file.write(nom)
        file.write(" :\t ")
        file.write(description)
        file.write("\n")
    print("\n!!! Sigle {} ajouté dans le fichier desciptions.txt du repertoire courant.".format(nom))


def option4():
    print("***************************************************************")
    print("*             4.Ajouter un sigle et sa définition             *")
    print("***************************************************************")
    description = input(" Entrer une description de sigle à ajouter: ")
    nom = input("Entrer son nom (EN MAJUSCULE) svp: ")
    # ligne = Sigle (nom, description)
    with open("descriptions.txt", "a") as file:
        file.write(nom)
        file.write(" : \t")
        file.write(description)
        file.write(str("\n"))
    print("\n!!! Sigle {} ajouté avec succes !!!".format(nom))
    return 0


def error():
    print("\n Choix non valide, réessayez")


# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


def main():
    print_hi('lOscillateur26')
    print("\t Application: Génération des SIGLES.")
    ans = True
    while ans:
        print("***************************************************************")
        print("#                    GENERATION DES SIGLES                    #")
        print("***************************************************************")
        menu_principal()
        ans = input("Faites un choix svp: ")

        if ans == "1":
            # 1 - Afficher les sigles
            option1()
            input("\n\t (appuyer sur ENTRER pour revenir au menu principal...) \n")
        elif ans == "2":
            # 2 - Réchercher un sigle
            option2()
            input("\n\t (appuyer sur ENTRER pour revenir au menu principal...) \n")
        elif ans == "3":
            # 3 - Générer/Déterminer un sigle:
            option3()
            print("***************************************************************")
            input("\n\t (appuyer sur ENTRER pour revenir au menu principal...) \n")
        elif ans == "4":
            # 1 - Ajouter un signe et sa définition:
            option4()
            print("***************************************************************")
            input("\n\t (appuyer sur ENTRER pour revenir au menu principal...) \n")
        elif ans == "5":
            print("\n\t Merci d'avoir utiliser notre application, aurevoir ...")
            break

        elif ans != "":
            error()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
    # Loscillateur26ftMathS
    # See PyCharm help at https://www.jetbrains.com/help/pycharm/
