from tkinter import *
# from tkinter.ttk import Button, Label, Entry
from tkinter import Button, Label, Entry


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


class MyWindow:
    def __init__(self, win):
        self.sigle = Label(win, text='SIGLE')
        self.textesigle = Entry(bd=2)
        self.sigle.place(x=65, y=50)
        self.textesigle.place(x=120, y=50)
        self.bR = Button(win, text=' Réchercher ', command=self.search)
        self.bR.place(x=260, y=47)
        self.description = Label(win, text='Description')
        self.description.place(x=50, y=100)
        self.textedescription = Entry(bd=2)
        self.textedescription.place(x=120, y=100, width=300)
        self.bG = Button(win, text=' Genérer sigle ',
                         command=self.generate)
        self.bG.place(x=425, y=97)
        self.bAjouter = Button(win, text=' Ajouter ce sigle ',
                               command=self.write,
                               font=('Times', 13),
                               bg='#4a7abd',
                               fg='yellow',
                               activebackground='green',
                               activeforeground='white')
        self.bAjouter.place(x=280, y=150)
        self.bAfficher = Button(win, text=' Afficher tous les sigles ',
                                command=self.show,
                                font=('Times', 13),
                                bg='yellow',
                                fg='#4a7abd',
                                activebackground='green',
                                activeforeground='white')
        self.bAfficher.bind(self.show)
        self.bAfficher.place(x=50, y=150)
        self.boutonInitialiser = Button(win, text=' Effacer les champs ',
                                        command=self.reset,
                                        font=('Times', 13),
                                        bg='#4a7abd', fg='yellow',
                                        activebackground='red',
                                        activeforeground='white')
        self.boutonInitialiser.place(x=450, y=150)
        self.lessigles = Text()
        self.lessigles.place(x=50, y=200, height=380, width=550)

    def search(self):
        elem = (self.textesigle.get())
        trouver = False
        if elem != " ":
            with open("descriptions.txt", "r", encoding='utf-8') as file:
                lines = file.readlines()
                # tab = []
                for line in lines:
                    tab = line.split(":")
                    if elem in tab[0]:
                        if elem != "":
                            trouver = True
                            result = tab[0] + " : " + tab[1]
                            self.lessigles.insert(END, result)
        else:
            result = " Entrée vide!!! "
        if not trouver:
            result = " pas trouvé :( \n"
            self.lessigles.insert(END, result)
            # print("\n Désolé, ce sigle n'existe pas ddans notre fichier.")

    def generate(self):
        description = str(self.textedescription.get())
        nom = ""
        if not description.isspace():
            for word in description:
                for char in word:
                    if char.isupper():
                        nom += char
                    else:
                        continue
        self.textesigle.insert(0, nom)
        self.write()

    def write(self):
        nom = str(self.textesigle.get())
        if not nom.isspace():
            if nom.isupper():
                description = str(self.textedescription.get())
                nd = nom + " : \t" + description
                line = nom + " : \t" + description + " \n "
                with open("descriptions.txt", "a", encoding='utf-8') as file:
                    file.write(line)
                # self.reset()
                self.lessigles.insert(END, nd)
                self.lessigles.insert(END, " (aoujté) \n")

    def show(self):
        with open("descriptions.txt", "r", encoding='utf-8') as file:
            content = file.read()
            self.lessigles.insert(END, content)

    def reset(self):
        self.textedescription.delete(0, END)
        self.textesigle.delete(1, END)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('lOscillateur26')
    window = Tk()
    mywin = MyWindow(window)
    window.title(' \t GESTION DES SIGLES')
    window.geometry("670x600")
    boutonQuitter = Button(window, text=' Quitter ', command=window.quit)
    window.mainloop()
